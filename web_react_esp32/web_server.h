
#ifndef _WEB_SERVER_H
#define _WEB_SERVER_H
#include <stdint.h>
#include "js_parser.h"

extern uint32_t json_string_length;
extern uint32_t json_tokens_length;
extern uint32_t config_binary_size;
extern uint32_t json_buffer_size;

esp_err_t web_server_init(const char *base_path, manual_t *cfg, wi_fi_t *wifi_cfg);

#endif