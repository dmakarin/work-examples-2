import React, { Component } from 'react';
// import { Tabs, Tab } from './Tabs';
import { TabPanel, a11yProps } from './TabsHelper';
import EspConfig from './EspConfig';
import EspLog from './EspLog';
import EspCharts from './EspCharts';
import EspScada from './EspScada';
import EspKSU from './EspKSU';
import { createMuiTheme, withStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { blue } from '@material-ui/core/colors';
import logo from './../assets/irz_tek_tech.svg';
import {
    Typography, Paper, CircularProgress, Divider, Hidden, AppBar,
    CssBaseline, Toolbar, List, Drawer, ListItem, ListItemText, ListItemIcon,
    IconButton, Tabs, Tab
} from '@material-ui/core';

import MenuIcon from '@material-ui/icons/Menu';

import { Alert, AlertTitle } from '@material-ui/lab';

// import TestingBackend from './TestingBackend';
let TESTING = false;
// Для отладки/тестирования на ПК

if (TESTING) {
    
    TestingBackend();
}

const theme = createMuiTheme({
    palette: {
        primary: blue,
        secondary: blue,
    },
    spacing: 4
});

const drawerWidth = 240;

const styles = them => ({
    root: {
        display: 'flex',
        flexGrow: 1,
        backgroundColor: them.palette.background.paper
    },
    drawer: {
        [them.breakpoints.up('lg')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        [them.breakpoints.up('lg')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: them.spacing(2),
        [them.breakpoints.up('lg')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar
    toolbar: them.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: them.spacing(3),
    },
});

class EspMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mobileOpen: false,
            tabIndex: 5,
            log: [""],
            config: null,
            lastMeasures: [],
            isConnectionSuccess: true,
            scadaLog: [],
            sysinfo: ""
        };
        this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
        this.scadaLogHandler = this.scadaLogHandler.bind(this);
    }

    handleDrawerToggle() {
        this.setState(prev => ({ mobileOpen: !prev.mobileOpen }));
    };

    componentDidMount() {
        this.configHandler();
        this.sysinfoHandler();
        this.id = setInterval(() => {
            this.logHandler();
            this.lastMeasuresHandler();
        }, 2000);
    }

    componentWillUnmount() {
        clearInterval(this.id);
    }

    lastMeasuresHandler() {
        return fetch("api/v1/lastMeasures", {
            method: 'GET', headers: {
                "Content-type": "text/plain; charset=UTF-8",
                "Authorization": sessionStorage.getItem('token')
            }
        })
            .then(res => {
                if (res.status == 400) {
                    this.props.authErrorHadler();
                }
                return res.ok ? res.text() : Promise.reject(res.text());
            })
            .then(
                (result) => {
                    this.setState({ lastMeasures: JSON.parse(result), isConnectionSuccess: true });
                },
                (error) => {
                    this.setState({ isConnectionSuccess: false });
                    console.log(error);
                }
            );
    }

    sysinfoHandler() {
        return fetch("api/v1/sysinfo", {
            method: 'GET', headers: {
                "Content-type": "text/plain; charset=UTF-8",
                "Authorization": sessionStorage.getItem('token')
            }
        })
            .then(res => {
                if (res.status == 400) {
                    this.props.authErrorHadler();
                }
                return res.ok ? res.text() : Promise.reject(res.text());
            })
            .then(
                (result) => {
                    this.setState({ sysinfo: JSON.parse(result), isConnectionSuccess: true });
                    console.log(this.state.sysinfo);
                },
                (error) => {
                    this.setState({ isConnectionSuccess: false });
                    console.log(error);
                }
            );
    }

    logHandler() {
        return fetch("api/v1/log", {
            method: 'GET', headers: {
                "Content-type": "text/plain; charset=UTF-8",
                "Authorization": sessionStorage.getItem('token')
            }
        })
            .then(res => {
                if (res.status == 400) {
                    this.props.authErrorHadler();
                }
                return res.ok ? res.text() : Promise.reject(res.text());
            })
            .then(
                (result) => {
                    let splitted = result.split(/\r?\n/);
                    splitted.splice(splitted.length - 1, 1);
                    if (this.state.log[this.state.log.length - 1].localeCompare(splitted[splitted.length - 1]) == 0) {
                        this.setState({ log: this.state.log, isConnectionSuccess: true })
                    }
                    else {
                        this.setState(prev => ({ log: prev.log.concat(splitted), isConnectionSuccess: true }));
                    }
                },
                (error) => {
                    this.setState({ isConnectionSuccess: false });
                    console.log(error);
                }
            );
    }

    configHandler() {
        return fetch("api/v1/config", {
            method: 'GET', headers: {
                "Content-type": "text/plain; charset=UTF-8",
                "Authorization": sessionStorage.getItem('token')
            }
        })
            .then(res => {
                if (res.status == 400) {
                    this.props.authErrorHadler();
                }
                return res.ok ? res.text() : Promise.reject(res.text());
            })
            .then(
                (result) => {
                    result = JSON.parse(result);
                    this.setState(prev => ({
                        isConnectionSuccess: true,
                        config: result
                    }))
                },
                (error) => {
                    this.setState({ isConnectionSuccess: false });
                    console.log(error);
                }
            );
    }

    scadaLogHandler(msg) {
        this.setState(prev => ({ scadaLog: prev.scadaLog.concat(msg) }));
    }

    authHanlder(isSuccess) {
        this.setState({ isAuthorized: isSuccess });
    }

    render() {
        if (this.state.config != null) {
            const { classes } = this.props;
            const indexChangeHandler = (event, newValue) => {
                this.setState({ mobileOpen: false, tabIndex: newValue });
            };
            const drawer = (
                <div>
                    <div className={classes.toolbar} />
                    <Divider />
                    <Tabs
                        variant="fullWidth"
                        onChange={indexChangeHandler}
                        value={this.state.tabIndex}
                        orientation='vertical'>
                        {['Параметры/Уставки', 'Графики', 'Журнал','Архив/Прошивка',
                        'WiFi Модуль Логи', 'WiFi Модуль Настройки']
                            .map((title, i) => (
                            <Tab label={title} key={i} {...a11yProps(i)}></Tab>
                        ))}
                    </Tabs>
                </div>
            );

            return (
                <ThemeProvider theme={theme}>
                    <div className={classes.root}>
                        <CssBaseline />
                        <AppBar position="fixed"
                            className={classes.appBar}>
                            <Toolbar>
                                <IconButton
                                    color="inherit"
                                    aria-label="open drawer"
                                    edge="start"
                                    size="small"
                                    onClick={this.handleDrawerToggle}
                                    className={classes.menuButton}
                                >
                                    <MenuIcon />
                                </IconButton>
                                {TESTING && <Typography variant="h5" >Тестовая среда!!!</Typography>}
                                {this.state.isConnectionSuccess && <img src={logo} />}
                                {this.state.isConnectionSuccess ||
                                    <Alert variant="filled" severity="error">
                                        <AlertTitle>Ошибка соединения</AlertTitle>
                                        Не удается осуществить запрос данных.
                                    </Alert>
                                }
                            </Toolbar>
                        </AppBar>
                        <nav className={classes.drawer}
                            aria-label="mailbox folders">
                            <Hidden lgUp implementation="css">
                                <Drawer
                                    variant="temporary"
                                    anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                                    open={this.state.mobileOpen}
                                    onClose={this.handleDrawerToggle}
                                    className={classes.drawerPaper}
                                    ModalProps={{
                                        keepMounted: true,
                                    }}
                                >
                                    {drawer}
                                </Drawer>
                            </Hidden>
                            <Hidden mdDown implementation="css">
                                <Drawer
                                    classes={{
                                        paper: classes.drawerPaper,
                                    }}
                                    variant="permanent"
                                    open>
                                    {drawer}
                                </Drawer>
                            </Hidden>
                        </nav>
                        <main className={classes.content}>
                            <div className={classes.toolbar} />
                            <TabPanel value={this.state.tabIndex} index={0}>
                                <EspScada lastMeasures={this.state.lastMeasures}
                                    polling={this.state.config.polling}
                                    scada={this.state.config.scada}
                                    logHandler={this.scadaLogHandler}></EspScada>
                            </TabPanel>
                            <TabPanel value={this.state.tabIndex} index={1}>
                                <EspCharts polling={this.state.config.polling}></EspCharts>
                            </TabPanel>
                            <TabPanel value={this.state.tabIndex} index={2}>
                                <EspLog log={this.state.scadaLog}></EspLog>
                            </TabPanel>
                            <TabPanel value={this.state.tabIndex} index={3}>
                                <EspKSU></EspKSU>
                            </TabPanel>
                            <TabPanel value={this.state.tabIndex} index={4}>
                                <EspLog log={this.state.log}></EspLog>
                            </TabPanel>
                            <TabPanel value={this.state.tabIndex} index={5}>
                                <EspConfig config={this.state.config} sysinfo={this.state.sysinfo}></EspConfig>
                            </TabPanel>
                            
                        </main>
                    </div >
                </ThemeProvider>);
        } else {
            return (
                <ThemeProvider theme={theme}>
                    <CircularProgress color='primary' size={140}></CircularProgress>
                </ThemeProvider >)
        }
    }
}

export default withStyles(styles)(EspMain);