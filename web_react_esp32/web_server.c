
#include "esp_log.h"
#include <esp_http_server.h>
#include <fcntl.h>
#include "esp_vfs.h"
#include "cJSON.h"
#include "web_server.h"
#include "js_parser.h"
#include "log_buffer.h"
#include "poll_service.h"
#include "poll.h"
#include "uart_wrap.h"
#include "version.h"
#include "time_util.h"
#include "token.h"
#include "ksu_util.h"
#include "mbedtls/base64.h"
#include "esp_spiram.h"

#define FILE_PATH_MAX (ESP_VFS_PATH_MAX + 128)

#define WEB_MAX_HTTP_CHUNK (1024 * 16)

static wi_fi_t *_wifi_cfg;
static char _base_path[64];

#define CHECK_FILE_EXTENSION(filename, ext) (strcasecmp(&filename[strlen(filename) - strlen(ext)], ext) == 0)

/*WEB API (request handlers)*/
static esp_err_t config_get_handler(httpd_req_t *req);
static esp_err_t config_post_handler(httpd_req_t *req);
static esp_err_t common_get_handler(httpd_req_t *req);
static esp_err_t log_get_handler(httpd_req_t *req);
static esp_err_t measures_get_handler(httpd_req_t *req);
static esp_err_t last_measures_get_handler(httpd_req_t *req);
static esp_err_t restart_post_handler(httpd_req_t *req);
static esp_err_t request_post_handler(httpd_req_t *req);
static esp_err_t sysinfo_get_handler(httpd_req_t *req);
static esp_err_t auth_post_handler(httpd_req_t *req);
static esp_err_t ksu_archive_get_handler(httpd_req_t *req);
static esp_err_t ksu_archive_info_get_handler(httpd_req_t *req);

uint32_t json_string_length;
uint32_t json_tokens_length;
uint32_t config_binary_size;
uint32_t json_buffer_size;

#define MAIN_CONTENT_RAM_SIZE (700 * 1024)
static int main_content_length;
static char *main_content_ram_clone;

static manual_t *_cfg;

static const char *TAG = "web-server";

static void get_file_name(const char *path, char *name)
{
    int len = strlen(path);
    char *ch = (char *)path + len;
    while (*ch != '/' && *ch != '\\' && ch != path)
    {
        printf("%c ", *ch);
        ch--;
    }
    ch++;
    strcpy(name, ch);
}

static uint8_t credential_is_valid(httpd_req_t *req, http_sess_token_t** _tok)
{
    char auth_str[256] = {0};
    if (httpd_req_get_hdr_value_str(req, "Authorization", auth_str, sizeof(auth_str)) == ESP_OK)
    {
        http_sess_token_t *tok = token_find_by_id(auth_str);
        if (_tok != NULL)
        {
            *_tok = tok;
        }
        
        if (tok == NULL)
        {
            return 0;
        }
        else
        {
            uint32_t now_sec = time_util_now_sec();
            if (token_is_time_expired(tok, now_sec, HTTP_SESS_TOKEN_LIFE_SEC))
            {
                token_free(tok);
                return 0;
            }
            else
            {
                token_refresh(tok, now_sec);
            }

            return 1;
        }
    }
    return 0;
}

static esp_err_t set_content_type_from_file(httpd_req_t *req, const char *filepath)
{
    const char *type = "text/plain";
    if (CHECK_FILE_EXTENSION(filepath, ".html"))
    {
        type = "text/html";
    }
    else if (CHECK_FILE_EXTENSION(filepath, ".js"))
    {
        type = "application/javascript";
    }
    else if (CHECK_FILE_EXTENSION(filepath, ".css"))
    {
        type = "text/css";
    }
    else if (CHECK_FILE_EXTENSION(filepath, ".png"))
    {
        type = "image/png";
    }
    else if (CHECK_FILE_EXTENSION(filepath, ".ico"))
    {
        type = "image/x-icon";
    }
    else if (CHECK_FILE_EXTENSION(filepath, ".svg"))
    {
        type = "text/xml";
    }
    else if (CHECK_FILE_EXTENSION(filepath, ".irz"))
    {
        type = "application/octet-stream";
    }
    return httpd_resp_set_type(req, type);
}

uint8_t ksu_archive_response[256] = {0};
uint32_t ksu_response_length = 0;

httpd_req_t *ksu_archive_req;
http_sess_token_t *ksu_archive_token;

static int bytes_to_hexstr(uint8_t *buffer, uint32_t len, char *str)
{
    str[0] = '\0';
    char tmp[8] = {0};
    for (uint32_t i = 0; i < len; i++)
    {
        sprintf(tmp, "%02X ", buffer[i]);
        strcat(str, tmp);
    }
    return 0;
}

static int ksu_util_write(uint8_t *buffer, uint32_t length)
{
    ksu_response_length = sizeof(ksu_archive_response);
    uart_wrap_execute_request_force(buffer, length, ksu_archive_response, &ksu_response_length);
    return length;
}

static int ksu_util_read(uint8_t *buffer, uint32_t length)
{
    if (ksu_response_length > 0)
    {
        memcpy(buffer, ksu_archive_response, length);
    }

    return ksu_response_length;
}

 // 672 очень важное "магическое число") для корректной кодировки chunk-посылки в base64 нужно,
 // чтобы размер исходной посылки был кратен 3 байтам
 // 72 - первая посылка (без имени и размера архива, 200 - 128)
 // 3 по 200 - еще 3 посылки
 // по итогу первый чанк - 672 байта, потом по 600. Размер последнего нам не важен
#define KSU_CACHE_SIZE (672)
uint8_t *ksu_archive_cache;
uint32_t ksu_archive_length = 0;

static int ksu_util_record(uint8_t *buffer, uint32_t length, uint8_t isLastRecord)
{
    uint32_t now_sec = time_util_now_sec();
    token_refresh(ksu_archive_token, now_sec);

    unsigned char base64[900] = {0};
    uint32_t base64_len = 0;

    if (isLastRecord)
    {
        if (ksu_archive_length > 0)
        {
            // По Http можно передать только 7 битные значения, поэтому часть символов не передается
            // для нужно кодировать в формат base64 (3 символа преобразуются в 4)
            if (mbedtls_base64_encode(base64, sizeof(base64), &base64_len,
                                      (const unsigned char *)ksu_archive_cache, ksu_archive_length) != 0)
            {
                ESP_LOGE(TAG, "base64 oversize %d", base64_len);
                return -1;
            };
            if (httpd_resp_send_chunk(ksu_archive_req, (const char *)base64, base64_len) != ESP_OK)
            {
                return -1;
            }
            ksu_archive_length = 0;
        }

        ESP_LOGW(TAG, "isLastRecord");
        httpd_resp_send_chunk(ksu_archive_req, NULL, 0);

        return length;
    }

    if (ksu_archive_length + length > KSU_CACHE_SIZE)
    {
        if (mbedtls_base64_encode(base64, sizeof(base64), &base64_len,
                                  (const unsigned char *)ksu_archive_cache, ksu_archive_length) != 0)
        {
            ESP_LOGE(TAG, "base64 oversize %d", base64_len);
            return -1;
        };

        if (httpd_resp_send_chunk(ksu_archive_req, (const char *)base64, base64_len) != ESP_OK)
        {
            return -1;
        }
        ksu_archive_length = 0;
    }

    memcpy(&ksu_archive_cache[ksu_archive_length], buffer, length);
    ksu_archive_length += length;

    return length;
}

static esp_err_t common_get_handler(httpd_req_t *req)
{
    char filepath[FILE_PATH_MAX] = {0};

    strcpy(filepath, _base_path);
    if (req->uri[strlen(req->uri) - 1] == '/')
    {
        strcat(filepath, "/index.html");
    }
    else
    {
        strcat(filepath, req->uri);
    }

    set_content_type_from_file(req, filepath);

    if (strcmp(filepath, "/storage/www/index-bundle.js") == 0 && esp_spiram_get_chip_size() == 2)
    {
        ESP_LOGW(TAG, "Попытка отправить index-bundle.js (размер %d)", main_content_length);
        httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
        uint32_t cur_pos = 0;
        while (1)
        {
            if (WEB_MAX_HTTP_CHUNK > main_content_length - cur_pos)
            {
                ESP_LOGI(TAG, "Файл index-bundle.js последний пакет");
                httpd_resp_send_chunk(req, (const char *)(main_content_ram_clone + cur_pos), main_content_length - cur_pos);
                ESP_LOGI(TAG, "Файл index-bundle.js отправлен");
                httpd_resp_send_chunk(req, NULL, 0);
                break;
            }
            else
            {
                if (httpd_resp_send_chunk(req, (const char *)(main_content_ram_clone + cur_pos), WEB_MAX_HTTP_CHUNK) != ESP_OK)
                {
                    ESP_LOGE(TAG, "Файл не отправлен");
                }
                else
                {
                    cur_pos += WEB_MAX_HTTP_CHUNK;
                }
            }
        }
        return ESP_OK;
    }

    int fd = open(filepath, O_RDONLY, 0);
    if (fd == -1)
    {
        strcat(filepath, ".gz");
        fd = open(filepath, O_RDONLY, 0);
        ESP_LOGI(TAG, "Попытка октрыть gzip файл: %s", filepath);
        if (fd == -1)
        {
            ESP_LOGE(TAG, "Попытка открыть gzip файл провалена: %s", filepath);
            httpd_resp_send_404(req);
            return ESP_FAIL;
        }
        else
        {
            httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
        }
    }

    char chunk[WEB_MAX_HTTP_CHUNK] = {0};
    ssize_t read_bytes;
    do
    {
        read_bytes = read(fd, chunk, WEB_MAX_HTTP_CHUNK);
        if (read_bytes == -1)
        {
            ESP_LOGE(TAG, "Ошибка чтения файла : %s", filepath);
        }
        else if (read_bytes > 0)
        {
            if (httpd_resp_send_chunk(req, chunk, read_bytes) != ESP_OK)
            {
                close(fd);
                ESP_LOGE(TAG, "Файл не отправлен!");
                httpd_resp_sendstr_chunk(req, NULL);
                httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Файл не отправлен!");
                return ESP_FAIL;
            }
        }
    } while (read_bytes > 0);
    close(fd);
    ESP_LOGI(TAG, "Файл отправлен");
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

esp_err_t web_server_init(const char *base_path, manual_t *cfg, wi_fi_t *wifi_cfg)
{
    _wifi_cfg = wifi_cfg;
    strcpy(_base_path, base_path);
    static int is_web_server_initialized = 0;
    if (is_web_server_initialized)
    {
        return ESP_FAIL;
    }
    is_web_server_initialized = 1;


    ksu_archive_cache = calloc(KSU_CACHE_SIZE, 1);

    _cfg = cfg;

    if(esp_spiram_get_chip_size() == 2)
    {
        main_content_ram_clone = calloc(MAIN_CONTENT_RAM_SIZE, 1);
        int fd = open("/storage/www/index-bundle.js.gz", O_RDONLY, 0);
        if (fd == -1)
        {
            return ESP_FAIL;
        }
        main_content_length = read(fd, main_content_ram_clone, MAIN_CONTENT_RAM_SIZE);
        if (main_content_length >= MAIN_CONTENT_RAM_SIZE)
        {
            return ESP_FAIL;
        }

        close(fd);
    }
    
    ESP_LOGI(TAG, "Основной Web контент успешно скопирован в RAM");

    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.uri_match_fn = httpd_uri_match_wildcard;
    config.stack_size = 1024 * 32;
    config.max_uri_handlers = 14;
    config.max_open_sockets = 12;
    config.backlog_conn = 0;

    ksu_set_read_callback(ksu_util_read);
    ksu_set_write_callback(ksu_util_write);

    httpd_start(&server, &config);

    ESP_LOGI(TAG, "HTTP Сервер запущен");

    httpd_uri_t logger_get_uri = {
        .uri = "/api/v1/log",
        .method = HTTP_GET,
        .handler = log_get_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &logger_get_uri));

    httpd_uri_t config_get_uri = {
        .uri = "/api/v1/config",
        .method = HTTP_GET,
        .handler = config_get_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &config_get_uri));

    httpd_uri_t config_post_uri = {
        .uri = "/api/v1/config/*",
        .method = HTTP_POST,
        .handler = config_post_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &config_post_uri));

    httpd_uri_t measures_get_uri = {
        .uri = "/api/v1/measures",
        .method = HTTP_GET,
        .handler = measures_get_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &measures_get_uri));

    httpd_uri_t last_measures_get_uri = {
        .uri = "/api/v1/lastMeasures",
        .method = HTTP_GET,
        .handler = last_measures_get_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &last_measures_get_uri));

    httpd_uri_t restart_post_uri = {
        .uri = "/api/v1/reboot",
        .method = HTTP_POST,
        .handler = restart_post_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &restart_post_uri));

    httpd_uri_t request_post_uri = {
        .uri = "/api/v1/request",
        .method = HTTP_POST,
        .handler = request_post_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &request_post_uri));

    httpd_uri_t sysinfo_get_uri = {
        .uri = "/api/v1/sysinfo",
        .method = HTTP_GET,
        .handler = sysinfo_get_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &sysinfo_get_uri));

    httpd_uri_t auth_post_uri = {
        .uri = "/api/v1/auth",
        .method = HTTP_POST,
        .handler = auth_post_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &auth_post_uri));

    httpd_uri_t ksu_archive_get_uri = {
        .uri = "/api/v1/ksu/archive.irz",
        .method = HTTP_GET,
        .handler = ksu_archive_get_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &ksu_archive_get_uri));

    httpd_uri_t ksu_archive_info_get_uri = {
        .uri = "/api/v1/ksu/archiveinfo",
        .method = HTTP_GET,
        .handler = ksu_archive_info_get_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &ksu_archive_info_get_uri));

    httpd_uri_t common_get_uri = {
        .uri = "/*",
        .method = HTTP_GET,
        .handler = common_get_handler};
    ESP_ERROR_CHECK(httpd_register_uri_handler(server, &common_get_uri));

    return ESP_OK;
}

static esp_err_t ksu_archive_info_get_handler(httpd_req_t *req)
{
    if (credential_is_valid(req, NULL) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }

    uint32_t size = 0;
    char file_name[125] = {0};
    char base64[125 * 2] = {0};
    uint32_t base64_len = 0;

    char *err = ksu_read_size(1, &size, file_name, sizeof(file_name));

    if (err != NULL)
    {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, err);
    }
    else
    {
        mbedtls_base64_encode((unsigned char *)base64, sizeof(base64), &base64_len,
                              (const unsigned char *)file_name, strlen(file_name));
        httpd_resp_send(req, base64, base64_len);
    }

    return ESP_OK;
}

static esp_err_t ksu_archive_get_handler(httpd_req_t *req)
{
    if (credential_is_valid(req, &ksu_archive_token) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }

    httpd_resp_set_type(req, "application/octet-stream");

    ksu_archive_req = req;

    if (uart_wrap_take_mutex(5000))
    {
        char *err = ksu_read_archive(1, 512 * 1024 * 1024, NULL, ksu_util_record);

        if (err != NULL)
        {
            ESP_LOGE(TAG, "Ошибка чтения архива: %s", err);
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, err);
        }
        else
        {
            httpd_resp_send_chunk(req, NULL, 0);
        }

        uart_wrap_give_mutex();
    }

    return ESP_OK;
}

static esp_err_t request_post_handler(httpd_req_t *req)
{
    if (credential_is_valid(req, NULL) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }

    char httpd_buffer[8128] = {0};

    int total_len = req->content_len;
    int cur_len = 0;
    int received = 0;
    if (total_len >= sizeof(httpd_buffer))
    {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Принятые данные больше выделенного буфера");
        return ESP_FAIL;
    }
    while (cur_len < total_len)
    {
        received = httpd_req_recv(req, httpd_buffer + cur_len, total_len);
        if (received <= 0)
        {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Принятые данные больше выделенного буфера");
            return ESP_FAIL;
        }
        cur_len += received;
    }
    httpd_buffer[total_len] = '\0';

    cJSON *obj = cJSON_Parse(httpd_buffer);
    if (obj == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, error_ptr);
            return ESP_OK;
        }
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "JSON ошибка чтения");
        return ESP_OK;
    }

    cJSON *id = cJSON_GetObjectItemCaseSensitive(obj, "id");
    if (!cJSON_IsNumber(id))
    {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "JSON ошибка [id]");
        cJSON_Delete(obj);
        return ESP_OK;
    }
    cJSON *args = cJSON_GetObjectItemCaseSensitive(obj, "args");
    if (!cJSON_IsArray(args))
    {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "JSON ошибка [args] ожидался массив");
        cJSON_Delete(obj);
        return ESP_OK;
    }

    uint32_t *arg_array = calloc(cJSON_GetArraySize(args), sizeof(uint32_t));
    for (uint32_t i = 0; i < cJSON_GetArraySize(args); i++)
    {
        cJSON *item = cJSON_GetArrayItem(args, i);
        if (!cJSON_IsNumber(item))
        {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "JSON ошибка [args] ожидался массив");
            cJSON_Delete(obj);
            free(arg_array);
            return ESP_OK;
        }
        arg_array[i] = item->valueint;
    }
    httpd_buffer[0] = '\0';
    char *err = poll_service_execute_request(httpd_buffer, sizeof(httpd_buffer), id->valueint, arg_array, cJSON_GetArraySize(args));
    if (err != NULL)
    {
        cJSON *http_res = cJSON_CreateObject();
        cJSON_AddStringToObject(http_res, "status", err);
        char *res_str = cJSON_PrintUnformatted(http_res);
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, res_str);

        cJSON_Delete(http_res);
        cJSON_free(res_str);
        cJSON_Delete(obj);
        cJSON_free(arg_array);
        return ESP_OK;
    }

    httpd_resp_sendstr(req, httpd_buffer);

    cJSON_Delete(obj);
    free(arg_array);
    return ESP_OK;
}

static esp_err_t auth_post_handler(httpd_req_t *req)
{
    char httpd_buffer[1024];
    int received = httpd_req_recv(req, httpd_buffer, sizeof(httpd_buffer));
    if (received <= 0 || received == sizeof(httpd_buffer))
    {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Принятые данные больше выделенного буфера");
        return ESP_FAIL;
    }

    cJSON *obj = cJSON_Parse(httpd_buffer);
    if (obj == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, error_ptr);
            return ESP_OK;
        }
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "JSON ошибка чтения");
        return ESP_OK;
    }

    cJSON *user = cJSON_GetObjectItemCaseSensitive(obj, "user");
    if (user == NULL || !cJSON_IsString(user))
    {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "JSON ошибка [user]");
        cJSON_Delete(obj);
        return ESP_OK;
    }
    cJSON *password = cJSON_GetObjectItemCaseSensitive(obj, "password");
    if (password == NULL || !cJSON_IsString(user))
    {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "JSON ошибка [password]");
        cJSON_Delete(obj);
        return ESP_OK;
    }

    if (user->valuestring == NULL || password->valuestring == NULL || strcmp(user->valuestring, _wifi_cfg->web->user) != 0 || strcmp(password->valuestring, _wifi_cfg->web->password) != 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }

    char newTokId[64] = {0};
    sprintf(newTokId, "%X%X%X%X", esp_random(), esp_random(), esp_random(), esp_random());
    char *err = token_add(newTokId, time_util_now_sec(), 15, 0);
    if (err)
    {
        ESP_LOGW(TAG, "Не удалось добавить токен - %s\r\n", err);
    }

    httpd_resp_send(req, newTokId, strlen(newTokId));
    return ESP_OK;
}

static esp_err_t sysinfo_get_handler(httpd_req_t *req)
{
    if (credential_is_valid(req, NULL) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }

    char *buf = calloc(1024 * 1, 1);
    version_sysinfo_json(buf);
    httpd_resp_sendstr(req, buf);
    return ESP_OK;
}

static esp_err_t restart_post_handler(httpd_req_t *req)
{
    if (credential_is_valid(req, NULL) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }
    httpd_resp_sendstr(req, "restart...");
    vTaskDelay(300 / portTICK_RATE_MS);
    esp_restart();

    return ESP_FAIL;
}

static esp_err_t last_measures_get_handler(httpd_req_t *req)
{
    if (credential_is_valid(req, NULL) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }
    char *buf = calloc(1024 * 8, 1);
    int res = poll_service_get_json_last_measures(buf, 1024 * 8);

    if (res < 0)
    {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Ошибка чтения последних данных");
    }
    else
    {
        httpd_resp_sendstr(req, buf);
    }

    free(buf);
    return ESP_OK;
}

static esp_err_t measures_get_handler(httpd_req_t *req)
{
    if (credential_is_valid(req, NULL) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }

    char *buf = calloc(1024 * 22 * 2, 1);
    int res = poll_service_get_json_measures(buf, 1024 * 22 * 2);
    if (res <= 0)
    {
        httpd_resp_sendstr(req, buf);
    }
    else
    {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Ошибка чтения измерений");
    }

    free(buf);

    return ESP_OK;
}

static esp_err_t config_get_handler(httpd_req_t *req)
{
    if (credential_is_valid(req, NULL) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }

    httpd_resp_set_type(req, "text/plain");
    
    char *config = calloc(json_string_length, 1);

    printf("heap_size %d", esp_get_free_heap_size());

    char full_path[64] = {0};
    char *config_base_path = "/storage/www/config/";

    DIR *dir = NULL;
    struct dirent *ent;
    dir = opendir("/storage/www/config");

    strcat(config, "{");
    while ((ent = readdir(dir)) != NULL)
    {
        char without_ext[32] = {0};
        strcat(without_ext, ent->d_name);
        without_ext[strlen(ent->d_name) - sizeof(".json") + 1] = '\0';

        char obj_name[260] = {0};
        sprintf(obj_name, "\"%s\":", without_ext);
        strcat(config, obj_name);

        ESP_LOGI(TAG, "file: %s", ent->d_name);
        *full_path = '\0';
        strcat(full_path, config_base_path);
        strcat(full_path, ent->d_name);

        int f = open(full_path, O_RDONLY, 0);
        if (f == -1)
        {
            free(config);
            ESP_LOGE(TAG, "Ошибка при открытии файла %s", full_path);
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Ошибка при открытии файла config.json");
            return ESP_FAIL;
        }
        int cur_length = strlen(config);
        int count = read(f, (void *)(config + cur_length), json_string_length - cur_length);

        // if (count + 3 >= buffer_size - strlen(config))
        if (count + cur_length == json_string_length) // TODO !!!
        {
            free(config);
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Файл больше выделенного буфера");
            return ESP_FAIL;
        }

        strcat(config, ",");
        close(f);
    }
    config[strlen(config) - 1] = '\0';
    strcat(config, "}");

    httpd_resp_sendstr(req, config);
    free(config);
    return ESP_OK;
}

static esp_err_t config_post_handler(httpd_req_t *req)
{
    http_sess_token_t *token = NULL;
    if (credential_is_valid(req, &token) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }

    ESP_LOGI(TAG, "config_post_handler");
    int total_len = req->content_len;
    int cur_len = 0;
    
    char *json_string_buffer = calloc(json_string_length, 1);
    jsmntok_t *jTokens = calloc(json_tokens_length, sizeof(jsmntok_t));
    uint8_t *config_buf = calloc(config_binary_size, 1);

    int received = 0;
    if (total_len >= json_string_length)
    {
        free(json_string_buffer);
        free(jTokens);
        free(config_buf);   
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Не хватает памяти");
        return ESP_FAIL;
    }
    while (cur_len < total_len)
    {
        token_refresh(token, time_util_now_sec());
        received = httpd_req_recv(req, json_string_buffer + cur_len, total_len);
        if (received <= 0)
        {
            free(json_string_buffer);
            free(jTokens);
            free(config_buf); 
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to post control value");
            return ESP_FAIL;
        }
        cur_len += received;
    }
    json_string_buffer[total_len] = '\0';

    jsmn_parser parser = {0};
    jsmn_init(&parser);
    mem_wrap_t config_mem = {0};
    
    memmgr_init(&config_mem, config_buf, config_binary_size);

    int result = jsmn_parse(&parser, json_string_buffer, total_len, jTokens, json_tokens_length);
    ESP_LOGI(TAG, "Результат парсинга json: %d", result);
    if (result < 0)
    {
        free(json_string_buffer);
        free(jTokens);
        free(config_buf); 
        ESP_LOGW(TAG, "Ошибка парсинга токенов jsmn: %s\r\n", jsmnerr_str[result + 3]);
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, jsmnerr_str[result + 3]);
        return ESP_FAIL;
    }

    JsonStream_t jStream = {0};
    jStream.jsonString = json_string_buffer;
    jStream.jsonStringLength = total_len;
    jStream.tokens = jTokens;
    jStream.tokenAmount = result;
    jStream.mem = &config_mem;

    char file_name[32] = {0};
    char config_base_path[FILE_PATH_MAX] = "/storage/www/config/";
    get_file_name(req->uri, file_name);
    strcat(config_base_path, file_name);

    char *err = NULL;

    if (strcmp(file_name, "retranslator.json") == 0)
    {
        retranslator_t cfg = {0};
        err = js_parse_retranslator(&cfg, &jStream);
    }
    if (strcmp(file_name, "wi_fi.json") == 0)
    {
        wi_fi_t cfg = {0};
        err = js_parse_wi_fi(&cfg, &jStream);
    }
    if (strcmp(file_name, "polling.json") == 0)
    {
        polling_t cfg = {0};
        err = js_parse_polling(&cfg, &jStream);
    }
    if (strcmp(file_name, "manual.json") == 0)
    {
        manual_t cfg = {0};
        err = js_parse_manual(&cfg, &jStream);
    }
    if (strcmp(file_name, "scada.json") == 0)
    {
        scada_t cfg = {0};
        err = js_parse_scada(&cfg, &jStream);
    }

    if (err != NULL)
    {
        free(json_string_buffer);
        free(jTokens);
        free(config_buf); 
        ESP_LOGI(TAG, "Ошибка чтения конфигурации: %s", err);
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, err);
        return ESP_FAIL;
    }

    int f = open(config_base_path, O_WRONLY, 0);
    if (f == -1)
    {
        free(json_string_buffer);
        free(jTokens);
        free(config_buf); 
        ESP_LOGE(TAG, "Ошибка открытия файл для записи");
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Ошибка открытия файл для записи");
        return ESP_FAIL;
    }
    write(f, json_string_buffer, total_len + 1);
    close(f);

    free(json_string_buffer);
    free(jTokens);
    free(config_buf);

    httpd_resp_sendstr(req, "Конфигурация удачно записана");

    return ESP_OK;
}

static esp_err_t log_get_handler(httpd_req_t *req)
{
    if (credential_is_valid(req, NULL) == 0)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Неверный логин или пароль");
        return ESP_FAIL;
    }

    httpd_resp_set_type(req, "text/plain");

    char *text = calloc(1024 * 4, 1);

    strcpy(text, log_buffer_get());

    httpd_resp_sendstr(req, text);
    free(text);

    return ESP_OK;
}