// Компонент для тестирования, подменяет функцию ajax запроса

import testConfigData from './test_config.json'
import archive from '../assets/archive_log.irz';
import axios from 'axios';

let counter = 0;
let valuesDictionary = {};
let token = undefined;
for (let i = 500; i < 600; i++) {
    valuesDictionary[i] = Math.round(Math.random() * 10) % 4;
}

let ABToStr = ab =>
    new Uint8Array(ab).reduce((p, c) =>
        p + String.fromCharCode(c), '');

export default function InitTestingBackend() {

    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        const { method, headers } = opts;
        const body = opts.body && JSON.parse(opts.body);

        return new Promise((resolve, reject) => {
            setTimeout(handleRoute, 250);

            function handleRoute() {
                switch (true) {
                    case url.endsWith('api/v1/log') && method === 'GET':
                        return logSimulator();
                    case url.endsWith('api/v1/config') && method === 'GET':
                        return configSimulator();
                    case url.endsWith('api/v1/measures') && method === 'GET':
                        return measuresSimulator();
                    case url.endsWith('api/v1/lastMeasures') && method === 'GET':
                        return lastMeasuresSimulator();
                    case url.endsWith('api/v1/sysinfo') && method === 'GET':
                        return sysinfoSimulator();
                    case url.endsWith('api/v1/request') && method === 'POST':
                        return requestSimulator(body);
                    case url.endsWith('api/v1/auth') && method === 'POST':
                        return authSimulator(body);
                    case url.endsWith('api/v1/ksu/archive.irz') && method === 'GET':
                        return ksuArchiveSimulator();
                    case url.endsWith('api/v1/ksu/archiveinfo') && method === 'GET':
                        return ksuArchiveInfoSimulator();
                    case url.endsWith('api/v1/state') && method === 'GET':
                        return stateSimulator();
                    default:
                        // pass through any requests not handled above
                        return realFetch(url, opts)
                            .then(response => resolve(response))
                            .catch(error => reject(error));
                }
            }

            function logSimulator() {
                let log = "";
                log = `${counter % 2 == 0 ? '\u001b[31m' : '\u001b[32m'}Test message ${counter}\u001b[0m\nTest message ${0 - counter}\n`;
                counter++;
                return ok(log);
            }

            function configSimulator() {
                return ok(JSON.stringify(testConfigData));
            }

            function measuresSimulator() {
                let measures = [];
                for (let i = 1; i < 13; i++) {
                    let time = 250;
                    for (let j = 0; j < time; j++) {
                        let id = i;
                        let timestamp = 50000 + time * 1000 - j * 1000;
                        let value = (Math.ceil(Math.random() * 150) + j / 10);
                        measures.push([id, timestamp, value]);
                    }
                }
                measures = Math.random() > 0.0 ? measures : [];
                // measures = [];
                return ok(JSON.stringify(measures));
            }

            function sysinfoSimulator() {
                let sysinfo = `{"esp32SoftVersion":"v0.0.1","esp32SoftDate":"Tue Sep 8 08:08:37 2020 +0400","esp32SoftDescription":"Egypt","webSoftVersion":"v0.0.1","staMacAddress":"4c:11:ae:df:a0:88","resetReason":"Reset due to power-on event","minimumHeap":2341196}`;
                return ok(sysinfo);
            }

            function authSimulator(body) {
                if (body.user == "admin" && body.password == "admin") {
                    token = Math.random().toString()
                    return ok(token);
                }
                return serverError("Ошибка авторизации");
            }

            function ksuArchiveSimulator() {
                return ok(btoa(ABToStr(archive)));
            }

            function ksuArchiveInfoSimulator() {
                let fileName = [0xCC, 0x2F, 0xF1, 0xE5, 0xEE, 0xF2, 0x33, 0xF0,
                    0xEA, 0x20, 0xF1, 0xF3, 0x35, 0xF2, 0xF1, 0x20, 0xE2, 0xEA,
                    0x36, 0x32, 0x20, 0x36, 0xD0, 0xC8, 0x2D, 0xC7, 0x30, 0x35,
                    0x20, 0x30, 0x30, 0xB9, 0x69, 0x2E, 0x7A, 0x72];

                return ok(btoa(ABToStr(fileName)));
            }

            function lastMeasuresSimulator() {
                let measures = [];
                for (let i = 1; i < 175; i++) {
                    let id = i;
                    let timestamp = Date.now();
                    let value = Math.ceil(Math.random() * 10) % 4;
                    measures.push([i, timestamp, value]);
                }
                let timestamp = Date.now();
                measures.push([207, timestamp, Math.ceil(Math.random() * 10) % 28]);
                measures.push([208, timestamp, Math.ceil(Math.random() * 10) % 146]);
                measures.push([209, timestamp, Math.ceil(Math.random() * 10) % 2]);
                measures.push([210, timestamp, Math.ceil(Math.random() * 10) % 1]);
                measures.push([211, timestamp, Math.ceil(Math.random() * 10) % 1]);

                return ok(JSON.stringify(measures));
            }

            function requestSimulator(body) {
                let response = {};
                let result = Math.random() > 0.5;
                response.status = result ? 'Успешно' : 'Ошибка: отсутствует Modbus регистр';
                response.measures = [];

                valuesDictionary[body.id] = body.args[0];

                if (body.id == 100) {
                    console.log('100 id request');
                    for (let i = 500; i < 600; i++) {
                        response.measures.push({ id: i, v: valuesDictionary[i] });
                    }
                }
                else {
                    response.measures.push({ id: body.id, v: valuesDictionary[body.id] });
                }

                return result ? ok(JSON.stringify(response)) : serverError(JSON.stringify(response));
            }

            // helper functions

            function ok(body) {
                resolve({ ok: true, text: () => body });
            }

            function unauthorized() {
                resolve({ status: 401, text: () => Promise.reject(JSON.stringify({ message: 'Unauthorized' })) });
            }

            function error(message) {
                resolve({ status: 400, text: () => Promise.reject(message) });
            }

            function serverError(message) {
                resolve({ status: 500, text: () => message });
            }

            function isLoggedIn() {
                return headers['Authorization'] === 'Bearer fake-jwt-token';
            }

            function idFromUrl() {
                const urlParts = url.split('/');
                return parseInt(urlParts[urlParts.length - 1]);
            }
        });
    }

    axios.get = (addr, cfg) => {
        console.log(addr);
        console.log(cfg);
        console.log(cfg);
        return new Promise((resolve, reject) => {
            let base64 = btoa(archive);
            cfg.onDownloadProgress(
                {
                    srcElement:{
                        response: base64.slice(0, 222222)
                    },
                    loaded: 222222 
                }
                );

            return resolve({ data: btoa(archive) });


        });
    };
}
