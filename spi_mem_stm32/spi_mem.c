/*
 * spi_mem.c
 *
 *  Created on: Feb 4, 2021
 *      Author: dmakarin
 */

#include <stdint.h>
#include <string.h>

#include <cmsis_os2.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

#include "spi_mem.h"

#include "spi.h"
#include "dma.h"
#include "gpio.h"

#include "stm32h7xx_hal_def.h"


#define SPI_MEM_STAT_WIP 1
#define SPI_MEM_STAT_WEL 2

#define SPI_MEM_CMD_WRITE_STATUS_REG 0x01
#define SPI_MEM_CMD_PAGE_PROGRAM 0x02
#define SPI_MEM_CMD_READ_DATA 0x03
#define SPI_MEM_CMD_WRITE_DISABLE 0x04
#define SPI_MEM_CMD_READ_STATUS_REG 0x05
#define SPI_MEM_CMD_WRITE_ENABLE 0x06
#define SPI_MEM_CMD_READ_HIGH_SPEED 0x0B
#define SPI_MEM_CMD_SECTOR_ERASE 0x20
#define SPI_MEM_CMD_BLOCK32K_ERASE 0x52
#define SPI_MEM_CMD_RESET_DEVICE 0xF0
#define SPI_MEM_CMD_READ_ID 0x9F

#define SPI_MEM_CMD_RELEASE_POWER_DOWN 0xAB
#define SPI_MEM_CMD_POWER_DOWN 0xB9
#define SPI_MEM_CMD_CHIP_ERASE 0xC7
#define SPI_MEM_CMD_BLOCK64K_ERASE 0xD8

typedef struct{
	int(*write_callback)(uint8_t* buffer, uint32_t length);
	uint32_t address;
	uint32_t length;
}SpiMemReadCommand_t;

typedef struct{
	uint32_t address;
	uint8_t* buffer;
	uint8_t length;
}SpiMemWriteCommand_t;

// spi chip select, CS low
#define SPI_MEM_SELECT() HAL_GPIO_WritePin(PIN_SELECT_MEM_1_GPIO_Port, PIN_SELECT_MEM_1_Pin, 0)
// spi chip deselect, CS high
#define SPI_MEM_DESELECT() HAL_GPIO_WritePin(PIN_SELECT_MEM_1_GPIO_Port, PIN_SELECT_MEM_1_Pin, 1)

//dma buffers place into dma_ram for cache correct work
ALIGN_32BYTES(uint8_t spi_mem_read_buffer1[SPI_MEM_DMA_BUFFER_SIZE]) __ALIGN_END  __attribute__((section(".dma_ram")));
ALIGN_32BYTES(uint8_t spi_mem_read_buffer2[SPI_MEM_DMA_BUFFER_SIZE]) __ALIGN_END __attribute__((section(".dma_ram")));
ALIGN_32BYTES(uint8_t spi_mem_write_buffer[SPI_MEM_PAGE_SIZE]) __ALIGN_END __attribute__((section(".dma_ram")));
ALIGN_32BYTES(uint8_t cmd_buffer[4]) __ALIGN_END __attribute__((section(".dma_ram")));

static uint8_t *pSpi_mem_buffer = spi_mem_read_buffer1;

static xQueueHandle qu_dma;
static xQueueHandle qu_read_cmd;
static xQueueHandle qu_write_cmd;
static xSemaphoreHandle mutex_dma;
static xSemaphoreHandle sem_write_complete;
static xSemaphoreHandle sem_read_complete;

static void spi_mem_wait();
static uint8_t spi_mem_read_status();
static void spi_mem_enable_write();
static void spi_mem_read_task(void* pvParameters);
static void spi_mem_write_task(void* pbParameters);

// wait access for write/erase
static void spi_mem_wait()
{
	while (spi_mem_read_status() & SPI_MEM_STAT_WIP)
		;
}

static uint8_t spi_mem_read_status()
{
	SPI_MEM_SELECT();

	cmd_buffer[0] = SPI_MEM_CMD_READ_STATUS_REG;

	uint8_t *pBuf = NULL;
	while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, 4) != HAL_OK)
		;
	xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

	SPI_MEM_DESELECT();

	return pBuf[1];
}

static void spi_mem_enable_write()
{
    SPI_MEM_SELECT();

    cmd_buffer[0] = SPI_MEM_CMD_WRITE_ENABLE;

	uint8_t *pBuf = NULL;
	while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, 1) != HAL_OK)
		;
	xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);
    SPI_MEM_DESELECT();
}

static void spi_mem_write_task(void* pbParameters)
{
	SpiMemWriteCommand_t cmd = {0};
	uint8_t *pBuf = NULL;

	uint32_t address = 0;

	while(1)
	{
		xQueuePeek(qu_write_cmd, &cmd, portMAX_DELAY);
		xSemaphoreTake(mutex_dma, portMAX_DELAY);
		xSemaphoreTake(sem_write_complete, 0);

		address = cmd.address;
		memcpy(spi_mem_write_buffer, cmd.buffer, cmd.length);

		xQueueReset(qu_write_cmd);

		spi_mem_wait();
		spi_mem_enable_write();

		SPI_MEM_SELECT();

		cmd_buffer[0] = SPI_MEM_CMD_PAGE_PROGRAM;
		cmd_buffer[1] = (uint8_t)(address >> 16);
		cmd_buffer[2] = (uint8_t)(address >> 8);
		cmd_buffer[3] = (uint8_t)(address);

		while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, 4) != HAL_OK)
			;
		xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

		while(HAL_SPI_TransmitReceive_DMA(&hspi4, spi_mem_write_buffer, pSpi_mem_buffer, cmd.length) != HAL_OK)
			;
		xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

		SPI_MEM_DESELECT();

		xSemaphoreGive(sem_write_complete);
		xSemaphoreGive(mutex_dma);
	}
}

static void spi_mem_read_task(void *pvParameters)
{
	SpiMemReadCommand_t cmd = {0};
	uint8_t *pBuf = NULL;

	while(1)
	{
		xQueuePeek(qu_read_cmd, &cmd, portMAX_DELAY);
		xSemaphoreTake(mutex_dma, portMAX_DELAY);
		xSemaphoreTake(sem_read_complete, 0);

		uint32_t address = cmd.address;
		uint32_t length = cmd.length;
		int(*write_callback)(uint8_t* buffer, uint32_t length) = cmd.write_callback;

		xQueueReset(qu_read_cmd);

		uint32_t next_position = 0;
		uint32_t size = 0;

		spi_mem_wait();

		SPI_MEM_SELECT();

		cmd_buffer[0] = SPI_MEM_CMD_READ_DATA;
		cmd_buffer[1] = (uint8_t)(address >> 16);
		cmd_buffer[2] = (uint8_t)(address >> 8);
		cmd_buffer[3] = (uint8_t)(address);

		while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, 4) != HAL_OK)
			;
		xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

		size = length < SPI_MEM_DMA_BUFFER_SIZE ? length : SPI_MEM_DMA_BUFFER_SIZE;

		while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, size) != HAL_OK)
			;
		next_position += size;

		while(1)
		{
			xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

			pSpi_mem_buffer = pSpi_mem_buffer == spi_mem_read_buffer1 ? spi_mem_read_buffer2 : spi_mem_read_buffer1;

			int diff = (int)length - next_position;
			if (diff > 0)
			{
				size = diff < SPI_MEM_DMA_BUFFER_SIZE ? diff : SPI_MEM_DMA_BUFFER_SIZE;

				while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, size) != HAL_OK)
					;
				next_position += size;
			}
			else
			{
				write_callback(NULL, 0);
				SPI_MEM_DESELECT();
				xSemaphoreGive(sem_read_complete);
				xSemaphoreGive(mutex_dma);
				break;
			}
		}
	}
}

void spi_mem_start()
{
	qu_dma = xQueueCreate(1, sizeof(uint8_t*));
	qu_read_cmd = xQueueCreate(1, sizeof(SpiMemReadCommand_t));
	qu_write_cmd = xQueueCreate(1, sizeof(SpiMemWriteCommand_t));
	mutex_dma = xSemaphoreCreateMutex();
	sem_write_complete = xSemaphoreCreateBinary();
	sem_read_complete = xSemaphoreCreateBinary();

	xTaskCreate(spi_mem_read_task, "spi_dma_read", 1024, NULL, osPriorityHigh, NULL);
	xTaskCreate(spi_mem_write_task, "spi_dma_write", 1024, NULL, osPriorityNormal, NULL);
}

void spi_mem_read_id(uint8_t* buffer, uint16_t length)
{
	cmd_buffer[0] = SPI_MEM_CMD_READ_ID;
	uint8_t *pBuf = NULL;

	SPI_MEM_SELECT();

	while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, length) != HAL_OK)
		;
	xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

	SPI_MEM_DESELECT();

	memcpy(buffer, pBuf, length);
}

void spi_mem_erase_chip()
{
	spi_mem_wait();
	spi_mem_enable_write();

	SPI_MEM_SELECT();

	cmd_buffer[0] = SPI_MEM_CMD_CHIP_ERASE;

	uint8_t *pBuf = NULL;
	while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, 1) != HAL_OK)
		;
	xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);
	SPI_MEM_DESELECT();
}

void spi_mem_erase_page_sector(uint32_t address)
{
	spi_mem_wait();
	spi_mem_enable_write();

	SPI_MEM_SELECT();

	cmd_buffer[0] = SPI_MEM_CMD_SECTOR_ERASE;
	cmd_buffer[1] = (uint8_t)(address >> 16);
	cmd_buffer[2] = (uint8_t)(address >> 8);
	cmd_buffer[3] = (uint8_t)(address);

	uint8_t *pBuf = NULL;
	while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, 4) != HAL_OK)
		;
	xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

	SPI_MEM_DESELECT();
}

void spi_mem_write_page(uint32_t address, uint8_t* buffer, uint8_t length)
{
	spi_mem_wait();
	spi_mem_enable_write();

	SPI_MEM_SELECT();

	cmd_buffer[0] = SPI_MEM_CMD_PAGE_PROGRAM;
	cmd_buffer[1] = (uint8_t)(address >> 16);
	cmd_buffer[2] = (uint8_t)(address >> 8);
	cmd_buffer[3] = (uint8_t)(address);

	memcpy(spi_mem_write_buffer, buffer, length);

	uint8_t *pBuf = NULL;
	while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, 4) != HAL_OK)
		;
	xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

	while(HAL_SPI_TransmitReceive_DMA(&hspi4, spi_mem_write_buffer, pSpi_mem_buffer, length) != HAL_OK)
		;
	xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

	SPI_MEM_DESELECT();
}

uint8_t spi_mem_write_wait(uint32_t delay)
{
	return xSemaphoreTake(sem_write_complete, delay/portTICK_RATE_MS);
}

void spi_mem_write_page_async(uint32_t address, uint8_t* buffer, uint8_t length)
{
	SpiMemWriteCommand_t cmd = {0};

	cmd.buffer = buffer;
	cmd.address = address;
	cmd.length = length;

	xQueueSend(qu_write_cmd, &cmd, portMAX_DELAY);
}

uint8_t spi_mem_read_wait(uint32_t delay)
{
	return xSemaphoreTake(sem_write_complete, delay/portTICK_RATE_MS);
}

void spi_mem_read_async(uint32_t address, uint32_t length, int (*write_callback)(uint8_t* buffer, uint32_t length))
{
	SpiMemReadCommand_t cmd = {0};
	cmd.address = address;
	cmd.length = length;
	cmd.write_callback = write_callback;

	xQueueSend(qu_read_cmd, &cmd, portMAX_DELAY);
}

void spi_mem_read(uint8_t* buffer, uint32_t address, uint16_t length)
{
	xSemaphoreTake(mutex_dma, portMAX_DELAY);
    spi_mem_wait();

	uint8_t *pBuf = NULL;

	SPI_MEM_SELECT();

	cmd_buffer[0] = SPI_MEM_CMD_READ_DATA;
	cmd_buffer[1] = (uint8_t)(address >> 16);
	cmd_buffer[2] = (uint8_t)(address >> 8);
	cmd_buffer[3] = (uint8_t)(address);

	while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, 4) != HAL_OK)
		;
	xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

	while(HAL_SPI_TransmitReceive_DMA(&hspi4, cmd_buffer, pSpi_mem_buffer, length) != HAL_OK)
		;
	xQueueReceive(qu_dma, &pBuf, portMAX_DELAY);

	memcpy(buffer, pSpi_mem_buffer, length);

	SPI_MEM_DESELECT();

	xSemaphoreGive(mutex_dma);
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xQueueSendFromISR(qu_dma, &hspi->pRxBuffPtr, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
