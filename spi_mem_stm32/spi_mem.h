/*
 * spi_mem.h
 *
 *  Created on: Feb 4, 2021
 *      Author: dmakarin
 */

// driver for spi memory chip W25Q16JVSSIQ, but may be working with another flash memory by JEDEC standart

#ifndef SPI_MEM_H_
#define SPI_MEM_H_

#define SPI_MEM_DMA_BUFFER_SIZE (1024*2)
#define SPI_MEM_PAGE_SIZE 256
#define SPI_MEM_SECTOR_SIZE 4096

// start freertos tasks for async functions
void spi_mem_start(void);
// read manufactured id
void spi_mem_read_id(uint8_t* buffer, uint16_t length);

// write page address should be multiply of SPI_MEM_PAGE_SIZE and maximum length SPI_MEM_PAGE_SIZE
void spi_mem_write_page(uint32_t address, uint8_t* buffer, uint8_t length);

// erase page sector, address should be multiply of SPI_MEM_PAGE_SIZE
void spi_mem_erase_page_sector(uint32_t address);

// read with result waiting
void spi_mem_read(uint8_t* buffer, uint32_t address, uint16_t length);
// read without result waiting
void spi_mem_read_async(uint32_t address, uint32_t length, int (*write_callback)(uint8_t* buffer, uint32_t length));
// wait result after spi_mem_read_async
uint8_t spi_mem_read_wait(uint32_t delay);
// write without write completing
void spi_mem_write_page_async(uint32_t address, uint8_t* buffer, uint8_t length);
// wait write completing, return 0 if uncompleted, else 1
uint8_t spi_mem_write_wait(uint32_t delay);

// full erase chip
void spi_mem_erase_chip(void);
void spi_mem_reset(void);

#endif /* SPI_MEM_H_ */
