# this script read sensors data from "28ВТ_калибровка.xlsx", calculate polynom's coefficients and
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as op
import glob
from jinja2 import Environment, FileSystemLoader
import xlrd
from datetime import datetime
import os

calibr_temperatures = [25, 100, 150, 200]
calibr_pressures = [0, 5, 10, 20, 40]


def parse_excel_source_dara(file_name):
    print("Trying read " + file_name + " ...")
    xls = xlrd.open_workbook(file_name)
    print("Read OK")
    sheet = xls.sheet_by_index(0)
    vals = [sheet.row_values(rownum) for rownum in range(sheet.nrows)]
    ht_device_start_row = 4
    ht_devices = []
    print("Trying parse " + file_name + " ...")
    for i in range(ht_device_start_row, vals.__len__(), 1):
        if vals[i][0] == '':
            break

        ht_dev = HtDevice(vals[i][0], vals[i][1], vals[i][2:],
                          calibr_temperatures, calibr_pressures)
        ht_devices.append(ht_dev)

        pass
    print("Parse OK")
    return ht_devices

# pressure's polynom


def fit_func(x_y, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11):
    F1, F2 = x_y
    return (a0 +
            a1*F2 +
            a2*F2*F2 +
            a3*F2*F2*F2 +
            a4*F1 +
            a5*F1*F2 +
            a6*F1*F2*F2 +
            a7*F1*F2*F2*F2 +
            a8*F1*F1 +
            a9*F1*F1*F2 +
            a10*F1*F1*F2*F2 +
            a11*F1*F1*F2*F2*F2)

# temperatures's polynom


def fit_func_temperature(x, a0, a1, a2):
    F1 = x
    return (a0 + a1*F1 + a2*F1*F1)


def calculate_coeffs_2d(preass, F1, F2):
    F2 = np.asarray(F2)
    F1 = np.asarray(F1)
    F1_forCompute = F1 - np.mean(F1).__int__()
    F2_forCompute = F2 - np.mean(F2).__int__()
    fitParams, fitCovariances = op.curve_fit(
        fit_func, (F1_forCompute, F2_forCompute), preass)
    return fitParams


def calculate_coeffs_1d(temperature, F1):
    F1 = np.asarray(F1)
    F1_forCompute = F1 - np.mean(F1).__int__()
    fitParams, fitCovariances = op.curve_fit(
        fit_func_temperature, F1_forCompute, temperature)
    return fitParams


class HtDevice:
    def __init__(self, serial, number, freqs, temperatures, pressures):
        temperature_sensor_count = 2
        pressure_sensor_count = 2

        def get_temp_data(start):
            temp_freqs = []
            step = pressures.__len__()*pressure_sensor_count + temperature_sensor_count
            for i in range(start, step*temperatures.__len__(), step):
                temp_freqs.append(freqs[i])
                pass
            return temperatures, temp_freqs

        def get_press_data(start, temp_start):
            press = []
            press_freqs1 = []
            press_freqs2 = []
            temp_step = pressures.__len__()*pressure_sensor_count + temperature_sensor_count
            for i in range(start, temp_step*temperatures.__len__(), temp_step):
                for j in range(i, i + pressures.__len__()*pressure_sensor_count, pressure_sensor_count):
                    press.append(pressures[((int)((j - i)/pressure_sensor_count) %
                                            pressures.__len__())])
                    press_freqs1.append(freqs[i - start + temp_start])
                    press_freqs2.append(freqs[j])
                    pass
            pass
            return press, press_freqs1, press_freqs2

        self.serial = serial.__int__()
        self.number = number.__int__()

        values, temp_freqs = get_temp_data(0)
        self.polynom_temp1 = calculate_coeffs_1d(values, temp_freqs)
        self.polynom_temp1_bias = 0 - np.mean(temp_freqs).__int__()
        values, temp_freqs = get_temp_data(1)
        self.polynom_temp2 = calculate_coeffs_1d(values, temp_freqs)
        self.polynom_temp2_bias = 0 - np.mean(temp_freqs).__int__()

        values, temp_freqs, press_freqs = get_press_data(2, 1)
        self.polynom_press1_temp1 = calculate_coeffs_2d(
            values, temp_freqs, press_freqs)

        self.polynom_press1_bias = 0 - np.mean(press_freqs).__int__()

        values, temp_freqs, press_freqs = get_press_data(2, 0)
        self.polynom_press1_temp2 = calculate_coeffs_2d(
            values, temp_freqs, press_freqs)

        values, temp_freqs, press_freqs = get_press_data(3, 1)
        self.polynom_press2_temp1 = calculate_coeffs_2d(
            values, temp_freqs, press_freqs)

        self.polynom_press2_bias = 0 - np.mean(press_freqs).__int__()

        values, temp_freqs, press_freqs = get_press_data(3, 0)
        self.polynom_press2_temp2 = calculate_coeffs_2d(
            values, temp_freqs, press_freqs)


def main():

    env = Environment(loader=FileSystemLoader(searchpath="./templates"))

    template = env.get_template('HT_Parameters_Coeffs.c.jinja')

    ht_devices = parse_excel_source_dara("28ВТ_калибровка.xlsx")

    text = template.render(ht_devices=ht_devices)
    File_c = open('HT_Parameters_Coeffs/HT_Parameters_Coeffs.c', 'w+')
    print(text, file=File_c)

    return


main()
