#ifndef HT_PARAMETERS_COEFFS_INCLUDE_H
#define HT_PARAMETERS_COEFFS_INCLUDE_H
#include <stdint.h>

typedef union {
    double f_64;
    uint32_t u_32;
    struct
    {
        uint16_t low;
        uint16_t hi;
    } u_16;
} U64_t;

typedef struct 
{
    U64_t* A; 
    float frequency;
    float value;
    int32_t constantFrequencyBias;
    uint16_t freqErrorCounter;
}HT_Parameter_t;

typedef struct 
{
    HT_Parameter_t* parameters;
    uint32_t serialNumber;
}HT_t;

extern HT_t hts[];
extern uint32_t HT_Count;

#endif //HT_PARAMETERS_COEFFS_INCLUDE_H
