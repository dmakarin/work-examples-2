import jsonschema as jsshm
import simplejson as json
from jinja2 import Environment, FileSystemLoader
import os

DEFAULT_MAX_STRING_LENGTH = 512
DEFAULT_MIN_STRING_LENGTH = 0
DEFAULT_MAX_ARRAY_OBJECT_SIZE = 512
DEFAULT_MIN_ARRAY_OBJECT_SIZE = 0
DEFAULT_MAX_INTEGER = 1000000
DEFAULT_MIN_INTEGER = -1000000
DEFAULT_MULTIPLE_OF = 1


def addAttributes(values):
    if values.get('type') != None and values['type'] == 'string':
        if values.get('maxLength') == None:
            values['maxLength'] = DEFAULT_MAX_STRING_LENGTH
        if values.get('minLength') == None:
            values['minLength'] = DEFAULT_MIN_STRING_LENGTH

    if values.get('type') != None and (values['type'] == 'integer' or values['type'] == 'number'):
        if values.get('maximum') == None:
            values['maximum'] = DEFAULT_MAX_INTEGER
        if values.get('minimum') == None:
            values['minimum'] = DEFAULT_MIN_INTEGER
        if values.get('multipleOf') == None:
            values['multipleOf'] = DEFAULT_MULTIPLE_OF


def parse_items(key, values, objects):
    addAttributes(values)

    if values.get('type') != None and values['type'] == 'object' and key != 'items':
        objects[key] = values

    if values.get('type') != None and values['type'] == 'array':
        if values.get('maxItems') == None:
            values['maxItems'] = DEFAULT_MAX_ARRAY_OBJECT_SIZE
        if values.get('minItems') == None:
            values['minItems'] = DEFAULT_MIN_ARRAY_OBJECT_SIZE

        # types
        addAttributes(values['items'])

        if values['items']['type'] == 'object':
            objects[key] = values['items']

    for k, v in values.items():
        if type(v) == dict:
            parse_items(k, v, objects)


def main():
    objects = {}
    schema = None
    file_name = 'react-web/src/components/schema.json'
    if os.sys.argv.__len__() > 1:
        file_name = os.sys.argv[1]

    schema_data = None
    schema = None

    with open(file_name, 'r', encoding='utf8') as f:
        schema_data = f.read()
        schema = json.loads(schema_data)
        jsshm.Draft7Validator.check_schema(schema)

    parse_items("config", schema, objects)

    # Reverse
    l = list()
    for k, v in objects.items():
        l.insert(0, (k, v))

    enumDict = {}

    for obj_name, obj_values in l:
        for prop_name, prop_value in obj_values['properties'].items():
            for prop_prop_name, prop_prop_value in prop_value.items():
                if prop_prop_name == 'enum':
                    enumDict[prop_name] = prop_prop_value

    env = Environment(loader=FileSystemLoader(searchpath="./"))

    template = env.get_template('config_type.c.jinja')
    
    with open('./js_parser.c', 'w') as f:
        f.write(template.render(objs=objects.items(), enumDict=enumDict.items()))

    template = env.get_template('config_type.h.jinja')
    with open('./js_parser.h', 'w') as f:
        f.write(template.render(objs=l, enumDict=enumDict.items()))
   
    print('OK')


main()
