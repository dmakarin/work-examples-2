#ifndef __MEM_WRAP_H_
#define __MEM_WRAP_H_

#include <stdint.h>
#include <stdlib.h>

typedef struct{
	uint8_t* start;
	uint32_t cur; 
	uint32_t size;
}mem_wrap_t;

void* memmgr_alloc(mem_wrap_t* mem, uint32_t size);

void memmgr_init(mem_wrap_t* mem, uint8_t* start_ptr, uint32_t size);

void memmgr_free(mem_wrap_t* mem);

#endif