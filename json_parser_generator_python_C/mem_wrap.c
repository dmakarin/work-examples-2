#include "mem_wrap.h"

void* memmgr_alloc(mem_wrap_t* mem, uint32_t size)
{
	if (size + mem->cur > mem->size)
	{
		return NULL;
	}
	void* res = &mem->start[mem->cur];
	mem->cur += size;
	
	return res;
}

void memmgr_init(mem_wrap_t* mem, uint8_t* start_ptr, uint32_t size)
{
	mem->start = start_ptr;
	mem->size = size;
	mem->cur = 0;
}

void memmgr_free(mem_wrap_t* mem)
{
	mem->cur = 0;
}